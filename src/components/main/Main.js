import React, { Component } from 'react';
import { } from 'react-native';
import Drawer from 'react-native-drawer';
//A3. File Main.js la giao dien chinh cua man hinh (gom file Menu ben trai va Shop)
// Main chua button OpenMenu drawer tu file Shop

// Import menu de co menu di toi cac trang khac.
import Menu from './Menu';
// Import shop de lam giao dien cho project.
import Shop from '../shop/Shop';

export default class Main extends Component {
    closeControlPanel = () => {
        this.drawer.close();
    };
    openControlPanel = () => {
        console.log(' ok open ');
        this.drawer.open();
    };
    render() {
        const { navigator } = this.props;
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={<Menu navigator={navigator} />}
                openDrawerOffset={0.4}
                tapToClose
            >

                <Shop open={this.openControlPanel.bind(this)} />
            </Drawer>
        );
    }
}
