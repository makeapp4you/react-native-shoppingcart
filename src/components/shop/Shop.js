import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import TabNavigator from 'react-native-tab-navigator';


//A4.2 Tai Shop chua giao dien cua project voi cac trang Home, cart, Search, Contact. 
// Chua TabNavigator de di toi cac trang Hom, Cart, Search, Contact.


// Giao dien chua cac trang home, cart, search, contact.
import Home from './home/Home';
import Cart from './cart/Cart';
import Header from './header';
import Search from './search/Search';
import Contact from './contact/Contact';

import icHome0 from '../images/icons/Home0.png';
import icHome from '../images/icons/Home.png';
import icCart from '../images/icons/Cart0.png';
import icCart0 from '../images/icons/Cart01.png';
import icSearch0 from '../images/icons/Search0.png';
import icSearch from '../images/icons/Search.png';
import icContact0 from '../images/icons/Contact0.png';
import icContact from '../images/icons/Contact.png';

export default class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedTab: 'home' };
    }
    goBackToMain() {
        const { navigator } = this.props;
        navigator.pop();
    }
    openMenu() {
        const { open } = this.props;
        open();
    }
    render() {
        const { iconStyle } = styles;
        return (
            <View style={{ flex: 1, backgroundColor: '#DCF0C8' }}>

                <Header onOpen={this.openMenu.bind(this)} />


                <TabNavigator>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'home'}
                        title="Home"
                        onPress={() => this.setState({ selectedTab: 'home' })}
                        renderIcon={() => <Image source={icHome0} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={icHome} style={iconStyle} />}
                    >
                        <Home />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'cart'}
                        title="Cart"
                        renderIcon={() => <Image source={icCart0} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={icCart} style={iconStyle} />}
                        onPress={() => this.setState({ selectedTab: 'cart' })}
                    >
                        <Cart />
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'search'}
                        title="Search"
                        renderIcon={() => <Image source={icSearch0} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={icSearch} style={iconStyle} />}
                        onPress={() => this.setState({ selectedTab: 'search' })}
                    >
                        <Search />
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'contact'}
                        title="Contact"
                        renderIcon={() => <Image source={icContact0} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={icContact} style={iconStyle} />}
                        onPress={() => this.setState({ selectedTab: 'contact' })}
                    >
                        <Contact />
                    </TabNavigator.Item>

                </TabNavigator>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    iconStyle: {
        width: 25, height: 25
    }
});
