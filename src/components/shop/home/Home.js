import React, { Component } from 'react';
import { Navigator } from 'react-native';
// A4.2.1. Tach trang home ra thanh trang HomeView - De lam trang Home la 1 navigator.
// Chua Navigator di toi cac trang HomeView, ListProduct, DetailProduct
// Tai Home khi use nhan 1 san pham se di toi trang List hoac trang Product tuong ung.
import HomeView from './HomeView';

import ListProduct from './products/List';
import DetailProduct from './products/Detail';



export default class Home extends Component {

    render() {
        return (
            <Navigator 
                initialRoute={{ name: 'HomeView' }}
                renderScene={(route, navigator) => {
                    switch (route.name) {
                        case 'HOME_VIEW': return <HomeView navigator={navigator} />;
                        case 'LIST_PRODUCT': return <ListProduct navigator={navigator} />;
                        case 'DETAIL_PRODUCT': return <DetailProduct navigator={navigator} />;
                        default: return <HomeView navigator={navigator} />;
                    }
                }}
            />
        );
    }
}
