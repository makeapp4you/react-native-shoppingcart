//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';

// import little from '../../images/temp/little.jpg';
import maxi from '../../images/temp/maxi.jpg';
import party from '../../images/temp/party.jpg';

const { width, height } = Dimensions.get('window');
// create a component

// Catergory Hien thi danh sach sp, khi nhan vao se toi trang ListProduct.
class Category extends Component {

    gotoListProduct() {
        const { navigator } = this.props;
        navigator.push({ name: 'LIST_PRODUCT' });
    }

    render() {
        const { wrapper, textStyle, imageStyle, cateTitle } = styles;
        return (
            <View style={wrapper}>
                <View style={{ justifyContent: 'center', flex: 1, paddingTop: 5 }}>
                    <Text style={textStyle} >LIST OF CATEGORY</Text>
                </View>
                <View style={{ justifyContent: 'flex-end', flex: 4 }}>
                    <Swiper showsPagination width={imageWidth} height={imageHeight} >
                        <TouchableOpacity onPress={this.gotoListProduct.bind(this)}>
                            <Image source={maxi} style={imageStyle}>
                                <Text style={cateTitle}>Maxi Dress</Text>
                            </Image>
                        </TouchableOpacity>
                         <TouchableOpacity onPress={this.gotoListProduct.bind(this)}> 
                            <Image source={party} style={imageStyle}>
                                <Text style={cateTitle}>Maxi Dress</Text>
                            </Image>
                        </TouchableOpacity>
                    </Swiper>
                </View>
            </View>
        );
    }
}
//933 x 465
const imageWidth = width - 40;
const imageHeight = imageWidth / 2;

// define your styles
const styles = StyleSheet.create({
    wrapper: {
        height: height * 0.35,
        backgroundColor: '#FFF',
        margin: 10,
        justifyContent: 'space-between',
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        padding: 10,
        paddingTop: 0
    },
    textStyle: {
        fontSize: 20,
        color: '#AFAEAF'
    },
    imageStyle: {
        height: imageHeight, 
        width: imageWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cateTitle: {
        fontSize: 20,
        fontFamily: 'Avenir',
        color: '#9A9A9A'
    }
});

//make this component available to the app
export default Category;
