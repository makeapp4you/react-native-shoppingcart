import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import Collection from './Collection';
import Category from './Category';

// Tai HomeView la noi hien thi giao dien cua <Collection /> <Category />, <TopProduct />

import TopProduct from './TopProduct';


export default class Home extends Component {

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <Collection navigator={this.props.navigator} />
                <Category navigator={this.props.navigator} />
                <TopProduct navigator={this.props.navigator} />
            </ScrollView>
        );
    }
}
