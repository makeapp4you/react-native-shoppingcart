//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';
import dress1 from '../../images/dress/dress1.jpg';

// get Dimension 
const { width, height } = Dimensions.get('window');

// create a component
class Collection extends Component {

    gotoListProduct() {
        const { navigator } = this.props;
        navigator.push({ name: 'LIST_PRODUCT' });
    }

    render() {
        const { container, imageStyle, textStyle } = styles;

        return (
            <View style={container}>
                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <Text style={textStyle}>PRING COLLECTION</Text>
                </View>

                <View style={{ flex: 4 }} >
                    <TouchableOpacity onPress={this.gotoListProduct.bind(this)}>
                        <ResponsiveImage source={dress1} style={imageStyle} />
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}
// define image dress
const imageWidth = width - 40;
const imageHeight = (imageWidth / 1200) * 696;

// define your styles
const styles = StyleSheet.create({
    container: {
        height: height * 0.38,
        backgroundColor: '#FFF',
        margin: 10,
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        padding: 10,
        paddingTop: 0
    },
    imageStyle: {
        height: imageHeight,
        width: imageWidth,
        marginBottom: 20
    },
    textStyle: {
        fontSize: 20,
        color: '#AFAEAF'
    }
});

//make this component available to the app
export default Collection;
