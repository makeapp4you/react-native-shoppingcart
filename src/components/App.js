import React, { Component } from 'react';
import { StatusBar, Navigator } from 'react-native';
// Day la man hinh duoc hien ra dau tien, dong thoi la noi de di toi cac trang khac cua app.
// A2. import Authentication, ChangeInfo, Main, OrderHistory
// use navigetor (initialRoute, renderScence)
import Authentication from './authentication/Authentication';
import ChangeInfo from './change-info/ChangeInfo';
import Main from './main/Main';
import OrderHistory from './order-history/OrderHistory';

StatusBar.setHidden(true);

export default class App extends Component {
    render() {
        return (
            <Navigator 
                initialRoute={{ name: 'MAIN' }}
                renderScene={(route, navigator) => {
                    switch (route.name) {
                        case 'MAIN': return <Main navigator={navigator} />;
                        case 'CHANGE_INFO': return <ChangeInfo navigator={navigator} />;
                        case 'AUTHENTICATION': return <Authentication navigator={navigator} />;
                        case 'ORDER_HISTORY': return <OrderHistory navigator={navigator} />;
                        default: return <OrderHistory navigator={navigator} />;
                    }
                }}
                configureScene={route => {
                    if (route.name === 'AUTHENTICATION') return Navigator.SceneConfigs.FloatFromRight;
                    return Navigator.SceneConfigs.FloatFromLeft;
                }}
            />
        );
    }
}
